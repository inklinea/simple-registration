# Simple Registration

Inkscape Extension, simple registration marks across objects. Possibly useful for bitmap scans or printing

Inkscape 1.1+

Appears Under 'Extensions>Render>Simple Registration'
